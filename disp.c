#include <stdio.h>

unsigned short menuComida(void);
unsigned asignarCosto(unsigned short opc);
void cambioMonedas(unsigned sald, unsigned cost);

int main(void){
	unsigned int saldo;
	unsigned short opcion;

	printf("%s", "Ingresa cantidad de dinero: $");
	scanf("%u", &saldo);

	if (saldo >= 200 && saldo <= 2500 && saldo % 100 == 0){

		opcion = menuComida();
		if (opcion >= 1 && opcion <= 8){
			unsigned costo = asignarCosto(opcion);

			if (costo <= saldo){
				cambioMonedas(saldo, costo);
			}

			else{
				puts("Error: No cuentas con saldo suficiente.");
				printf("Devolviendo saldo: $%u\n", saldo);
			}
		}

		else if (opcion == 9){
			puts("Saliendo...");
			printf("Devolviendo saldo: $%u\n", saldo);
		}

		else{
			printf("Error: %u no es una opción válida\n", opcion);
			printf("Devolviendo saldo: $%u\n", saldo);
		}
	}

	else{
		printf("Error: el monto $%d no es válido\n", saldo);
		printf("Devolviendo saldo: $%u\n", saldo);
	}
}

unsigned short menuComida(void){
	unsigned short op;

	puts("Máquina Dispensadora “El Puente”");
	puts("1 Papas Fritas          $1200");
	puts("2 Sándwich Combinado    $2500");
	puts("3 Pescadito             $1800");
	puts("4 Empanada              $1700 ");
	puts("5 Arepa                 $2000");
	puts("6 Gaseosa               $1600");
	puts("7 Vaso de Té            $1000");
	puts("8 Dulce                  $200");
	puts("9 Salir");

	printf("%s", "Digite su opción > ");
	scanf("%hu", &op);

	return op;
}

unsigned asignarCosto(unsigned short opc){
	unsigned cost;

	switch(opc){
		case 1:
			cost = 1200;
			break;
		case 2:
			cost = 2500;
			break;
		case 3:
			cost = 1800;
			break;
		case 4:
			cost = 1700;
			break;
		case 5:
			cost = 2000;
			break;
		case 6:
			cost = 1600;
			break;
		case 7:
			cost = 1000;
			break;
		case 8:
			cost = 200;
			break;
	}

	return cost;
}

void cambioMonedas(unsigned sald, unsigned cost){

	unsigned short m500;
	unsigned short m200;
	unsigned short m100;

	sald -= cost;

	puts("Entregando producto...");
	printf("%s\t%s\n", "Moneda", "Cantidad");

	m500 = sald / 500;
	sald %= 500;
	printf("$500\t%hu\n", m500);

	m200 = sald / 200;
	sald %= 200;
	printf("$200\t%hu\n", m200);

	m100 = sald / 100;
	sald %= 100;
	printf("$100\t%hu\n", m100);
}
